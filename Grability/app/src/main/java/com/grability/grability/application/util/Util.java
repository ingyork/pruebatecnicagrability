package com.grability.grability.application.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.grability.grability.GrabilityApplication;

/**
 * Created by jorge.castro on 12/28/2017.
 */

public class Util {

    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) GrabilityApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
