package com.grability.grability.view.filmList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.grability.grability.R;
import com.grability.grability.data.model.Film;
import com.grability.grability.presenter.filmList.FilmListPresenter;
import com.grability.grability.presenter.filmList.IFilmListPresenter;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.view.BaseActivity;
import com.grability.grability.view.detailFilm.DetailFilmActivity;
import com.grability.grability.view.detailFilm.DetailFilmListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilmListActivity extends BaseActivity implements IFilmListView, DetailFilmListener {

    static final int REQUEST_CODE_SUCCESS = 1;

    private FilmListAdapter adapter;
    IFilmListPresenter presenter;

    @BindView(R.id.rvFilms) RecyclerView recyclerViewFilms;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.tvMessageProgressbar)
    TextView tvMessageProgressbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);
        ButterKnife.bind(this);

        init();
    }

    private void init(){

        recyclerViewFilms.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewFilms.setHasFixedSize(true);

        presenter = new FilmListPresenter(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String categoryId = bundle.getString(PreferencesUtils.KeyPreferences.CATEGORY_ID);
            String categoryName = bundle.getString(PreferencesUtils.KeyPreferences.CATEGORY_NAME);

            setTitle(categoryName);
            presenter.loadFilmsByCategory(Integer.parseInt(categoryId));
        }

    }

    @Override
    public void loadFilmByCategory(List<Film> films) {
        adapter = new FilmListAdapter(films);
        adapter.setCallbackFilmListAdapter(this);
        recyclerViewFilms.setAdapter(adapter);
        showRecyclerviewFilmList();
    }



    private void showRecyclerviewFilmList(){
        progressBar.setVisibility(View.GONE);
        tvMessageProgressbar.setVisibility(View.GONE);
        recyclerViewFilms.setVisibility(View.VISIBLE);
    }

    @Override
    public void launchActivityForDetail(long filmId) {
        Intent intent = new Intent(this, DetailFilmActivity.class);
        intent.putExtra(PreferencesUtils.KeyPreferences.FILM_ID, String.valueOf(filmId));
        startActivityForResult(intent, REQUEST_CODE_SUCCESS);
    }
}
