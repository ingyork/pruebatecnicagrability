package com.grability.grability.Interactor.detailFilm;

import com.grability.grability.data.services.MovieRetrofitFactory;
import com.grability.grability.data.services.MovieService;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.application.util.Prefs;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public class DetailFilmInteractor implements IDetailFilmInteractor {

    OnDetailFilmListener listener;

    @Override
    public void setListener(OnDetailFilmListener listener) {
        this.listener = listener;
    }

    @Override
    public void showDetailFilm(long filmId) {

        String apiKey = Prefs.getString(PreferencesUtils.KeyPreferences.API_KEY, "");

        MovieService service = MovieRetrofitFactory.create();
        Disposable disposable = service.getDetailMovie(filmId, apiKey)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(film -> {
                    listener.onDetailFilmListener(film);
                });
    }

}