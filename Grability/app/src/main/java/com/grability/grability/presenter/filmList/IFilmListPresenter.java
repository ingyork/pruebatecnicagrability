package com.grability.grability.presenter.filmList;

import com.grability.grability.presenter.BasePresenter;
import com.grability.grability.view.filmList.IFilmListView;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public interface IFilmListPresenter extends BasePresenter<IFilmListView> {
    void loadFilmsByCategory(int categoryId);
}
