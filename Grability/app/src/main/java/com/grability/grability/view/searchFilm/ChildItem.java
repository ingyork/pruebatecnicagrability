package com.grability.grability.view.searchFilm;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class ChildItem {
    long id;
    String name;
    String imgCoverPath;
    double voteAverage;

    public ChildItem(long id, String name, String imgCoverPath, double voteAverage) {
        this.id = id;
        this.name = name;
        this.imgCoverPath = imgCoverPath;
        this.voteAverage = voteAverage;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImgCoverPath(){ return imgCoverPath; }

    public double getVoteAverage(){ return voteAverage; }
}
