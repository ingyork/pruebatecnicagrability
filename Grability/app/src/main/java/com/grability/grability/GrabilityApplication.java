package com.grability.grability;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;


import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.application.util.Prefs;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public class GrabilityApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        GrabilityApplication.context = getApplicationContext();

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(PreferencesUtils.KeyPreferences.PREFERENCE_NAME)
                .setUseDefaultSharedPreference(false)
                .build();

        PreferencesUtils.updateApiKey("51b6d3e433484ae65de2e3e73e92e939");
    }


    public static Context getAppContext() {
        return GrabilityApplication.context;
    }
}
