package com.grability.grability.view.searchFilm;

import com.intrusoft.sectionedrecyclerview.Section;

import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class SectionHeader implements Section<ChildItem> {

    List<ChildItem> childList;
    String sectionText;

    public SectionHeader(List<ChildItem> childList, String sectionText) {
        this.childList = childList;
        this.sectionText = sectionText;
    }

    @Override
    public List<ChildItem> getChildItems() {
        return childList;
    }

    public String getSectionText() {
        return sectionText;
    }
}
