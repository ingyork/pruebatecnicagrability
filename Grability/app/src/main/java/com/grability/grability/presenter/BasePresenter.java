package com.grability.grability.presenter;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public interface BasePresenter<T> {
    void addView(T view);
    void removeView();
}