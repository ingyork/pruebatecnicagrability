package com.grability.grability.data.model;

import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class FilmGroup {

    private String groupName;
    private List<Film> films;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }
}
