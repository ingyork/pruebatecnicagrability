package com.grability.grability.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.grability.grability.data.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class CategoryDB extends BaseEntities {

    public static String TABLE_NAME = "category";
    public static final String QUERY_CREATE = "CREATE TABLE "+TABLE_NAME+"(" +
                                                " "+Column.ID+" INTEGER PRIMARY KEY," +
                                                " "+Column.NAME+" TEXT NOT NULL," +
                                                " "+Column.IMAGE_RESOURCE+" INTEGER NOT NULL)";

    public class Column {
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String IMAGE_RESOURCE = "image_resource";
    }


    public CategoryDB(Context context) {
        super(context);
    }


    public boolean checkEmpty(){
        int count = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME, null);
        try {
            if(cursor != null)
                if(cursor.getCount() > 0){
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
        }finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        if(count>0)
            return false;
        else
            return true;
    }



    public boolean deleteTable(){
        boolean response = true;
        try{
            this.openWriteableDB();
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e){
            response = false;
        }
        finally {
            this.closeDB();
        }
        return response;
    }



    public ArrayList<Category> getCategories() {
        ArrayList list = new ArrayList<Category>();
        this.openReadableDB();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);

        try {
            while (c.moveToNext()) {
                Category category = new Category();
                category.setId(c.getInt(0));
                category.setName(c.getString(1));
                category.setImageResource(c.getInt(2));
                list.add(category);
            }
        } finally {
            c.close();
            this.closeDB();
        }
        return list;
    }

    public void saveBatch(List<Category> categories) {
        deleteTable();
        new SaveBatchTask(db, categories).execute();
    }



    private ContentValues categoryMapperContentValues(Category category) {
        ContentValues cv = new ContentValues();
        cv.put(Column.ID, category.getId());
        cv.put(Column.NAME, category.getName());
        cv.put(Column.IMAGE_RESOURCE, category.getImageResource());
        return cv;
    }


    private class SaveBatchTask extends AsyncTask<Void, Void, Boolean> {

        private SQLiteDatabase db;
        private List<Category> categories;

        public SaveBatchTask(SQLiteDatabase db, List<Category> categories){
            this.db = dbHelper.getReadableDatabase();
            this.categories = categories;
        }

        @Override
        protected Boolean doInBackground(Void... args) {
            boolean response = true;
            for(Category c : categories){
                long rowID = db.insert(TABLE_NAME, null, categoryMapperContentValues(c));
            }
            return response;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(db!=null){
                db.close();
            }
        }

    }


}
