package com.grability.grability.Interactor.searchFilm;

import com.grability.grability.data.model.GroupFilmGenre;

import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public interface ISearchFilmInteractor {
    public interface OnListenerSearchFilm{
        void onSuccessSearchFilms(List<GroupFilmGenre> groupFilmGenres);
    }

    void setListener(OnListenerSearchFilm listener);
    void searchFilms(String searchPattern);
}
