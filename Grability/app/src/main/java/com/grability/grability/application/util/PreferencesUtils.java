package com.grability.grability.application.util;

/**
 * Created by jorge.castro on 11/18/2017.
 */

public class PreferencesUtils {

    public class KeyPreferences {
        public static final String PREFERENCE_NAME = "applicationPreferences";
        public static final String CATEGORY_ID = "categoryId";
        public static final String CATEGORY_NAME = "categoryName";
        public static final String API_KEY = "apiKey";

        public static final String FILM_ID = "filmId";
    }

    public static void updateApiKey(String apiKey){
        Prefs.putString(KeyPreferences.API_KEY, apiKey);
    }

    @Override
    public PreferencesUtils clone(){
        try {
            throw new CloneNotSupportedException();
        }catch(CloneNotSupportedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
