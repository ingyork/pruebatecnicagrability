package com.grability.grability.data.services;

import com.grability.grability.data.services.MovieService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public class MovieRetrofitFactory {

    static String BASE_URL = "https://api.themoviedb.org/3/movie/";
    static String BASE_URL_SEARCH = "https://api.themoviedb.org/3/search/";
    static String BASE_URL_GENRES = "https://api.themoviedb.org/3/genre/movie/";


    private static Retrofit.Builder retrofit;

    /*public static <T> T create(final Class<T> clazz) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MovieRetrofitFactory.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(clazz);
    }*/



    /*public static <T> T createSearch(final Class<T> clazz) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL_SEARCH)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(clazz);
    }*/



    public static MovieService create() {
        initRetrofit();
        retrofit.baseUrl(BASE_URL);
        return retrofit.build().create(MovieService.class);
    }


    public static MovieService createGenres() {
        initRetrofit();
        retrofit.baseUrl(BASE_URL_GENRES);
        return retrofit.build().create(MovieService.class);
    }

    public static MovieService createSearch() {
        initRetrofit();
        retrofit.baseUrl(BASE_URL_SEARCH);
        return retrofit.build().create(MovieService.class);
    }


    private static void initRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

            retrofit.client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        }
    }
}
