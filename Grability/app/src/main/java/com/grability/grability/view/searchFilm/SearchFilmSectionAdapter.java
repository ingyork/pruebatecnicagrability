package com.grability.grability.view.searchFilm;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.grability.grability.R;
import com.grability.grability.view.detailFilm.DetailFilmListener;
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class SearchFilmSectionAdapter extends SectionRecyclerViewAdapter<SectionHeader, ChildItem, SearchFilmSectionAdapter.SectionViewHolder, SearchFilmSectionAdapter.ChildViewHolder> {


    DetailFilmListener listener;
    Context context;

    public SearchFilmSectionAdapter(Context context, List<SectionHeader> sectionItemList) {
        super(context, sectionItemList);
        this.context = context;
    }

    public void setListener(DetailFilmListener listener){
        this.listener = listener;
    }

    @Override
    public SectionViewHolder onCreateSectionViewHolder(ViewGroup sectionViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_searchfilm_header, sectionViewGroup, false);
        return new SectionViewHolder(view);
    }

    @Override
    public ChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_searchfilm_child, childViewGroup, false);
        return new ChildViewHolder(view);
    }

    @Override
    public void onBindSectionViewHolder(SectionViewHolder sectionViewHolder, int sectionPosition, SectionHeader section) {
        sectionViewHolder.name.setText(section.sectionText);
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder childViewHolder, int i, int i1, ChildItem childItem) {

        childViewHolder.txtId.setText(String.valueOf(childItem.getId()));
        childViewHolder.name.setText(childItem.getName());

        double rating = childItem.getVoteAverage() * 10 / 20;
        childViewHolder.ratingBar.setRating((float)rating);


        Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500" + childItem.getImgCoverPath());
        Picasso.with(context).load(uri)
                .into(childViewHolder.imgCover);
    }


    public class SectionViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public SectionViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.section);
        }
    }


    public class ChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtId;
        TextView name;
        ImageView imgCover;
        RatingBar ratingBar;

        public ChildViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            txtId = itemView.findViewById(R.id.txtId);
            name = itemView.findViewById(R.id.child);
            imgCover = itemView.findViewById(R.id.imgCover);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }

        @Override
        public void onClick(View view) {
            long filmId = Integer.parseInt(txtId.getText().toString());
            listener.launchActivityForDetail(filmId);
        }
    }
}
