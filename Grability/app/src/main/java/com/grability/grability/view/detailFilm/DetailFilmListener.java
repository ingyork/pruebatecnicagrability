package com.grability.grability.view.detailFilm;

import com.grability.grability.data.model.Film;

/**
 * Created by jorge.castro on 1/7/2018.
 */

public interface DetailFilmListener {
    void launchActivityForDetail(long filmId);
}
