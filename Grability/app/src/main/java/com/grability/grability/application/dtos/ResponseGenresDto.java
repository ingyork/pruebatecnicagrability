package com.grability.grability.application.dtos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.grability.grability.data.model.Genre;

import java.util.List;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public class ResponseGenresDto {

    @SerializedName("genres")
    @Expose
    private List<Genre> genres;

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }
}
