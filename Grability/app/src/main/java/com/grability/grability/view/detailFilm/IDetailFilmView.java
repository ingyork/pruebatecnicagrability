package com.grability.grability.view.detailFilm;

import com.grability.grability.data.model.Film;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public interface IDetailFilmView {
    void showDetailFilm(Film film);
}
