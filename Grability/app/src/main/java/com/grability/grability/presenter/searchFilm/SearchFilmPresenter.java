package com.grability.grability.presenter.searchFilm;

import com.grability.grability.Interactor.searchFilm.ISearchFilmInteractor;
import com.grability.grability.Interactor.searchFilm.SearchFilmInteractor;
import com.grability.grability.data.model.GroupFilmGenre;
import com.grability.grability.view.searchFilm.ISearchFilmView;

import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class SearchFilmPresenter implements ISearchFilmPresenter, ISearchFilmInteractor.OnListenerSearchFilm {

    private ISearchFilmView view;
    private ISearchFilmInteractor interactor;

    public SearchFilmPresenter(ISearchFilmView view){
        addView(view);
        interactor = new SearchFilmInteractor();
        interactor.setListener(this);
    }

    @Override
    public void addView(ISearchFilmView view) {
        this.view = view;
    }

    @Override
    public void removeView() {

    }

    @Override
    public void searchFilms(String searchPattern) {
        interactor.searchFilms(searchPattern);
    }

    @Override
    public void onSuccessSearchFilms(List<GroupFilmGenre> groupFilmGenres) {
        view.showFilms(groupFilmGenres);
    }
}
