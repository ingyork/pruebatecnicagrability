package com.grability.grability.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.grability.grability.data.model.Film;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class FilmDB extends BaseEntities {

    public static String TABLE_NAME = "film";
    public static final String QUERY_CREATE = "CREATE TABLE "+TABLE_NAME+"(" +
            " "+ Column.ID+" INTEGER PRIMARY KEY," +
            " "+ Column.CATEGORY_ID+" INTEGER," +
            " "+ Column.TITLE+" TEXT," +
            " "+ Column.VOTE_COUNT+" INTEGER," +
            " "+ Column.VIDEO+" INTEGER," +
            " "+ Column.VOTE_AVERAGE+" REAL," +
            " "+ Column.POPULARITY+" REAL," +
            " "+ Column.POSTER_PATH+" TEXT," +
            " "+ Column.BACKDROP_PATH+" TEXT,"+
            " "+ Column.ORIGINAL_LANGUAGE+" TEXT," +
            " "+ Column.ORIGINAL_TITLE+" TEXT," +
            " "+ Column.OVERVIEW+" TEXT," +
            " "+ Column.RELEASE_DATE+" TEXT," +
            " "+ Column.HOMEPAGE+" TEXT);";


    public class Column {
        public static final String ID = "_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String TITLE = "title";
        public static final String VOTE_COUNT = "vote_count";
        public static final String VIDEO = "video";
        public static final String VOTE_AVERAGE = "vote_average"; //real
        public static final String POPULARITY = "popularity";
        public static final String POSTER_PATH = "poster_path";
        public static final String BACKDROP_PATH = "backdrop_path";
        public static final String ORIGINAL_LANGUAGE = "original_language";
        public static final String ORIGINAL_TITLE = "original_title";
        public static final String OVERVIEW = "overview";
        public static final String RELEASE_DATE = "release_date";
        public static final String HOMEPAGE = "homepage";
        /*
        *
    @SerializedName("genre_ids")
    @Expose
    private List<Integer> genreIds = null;
    @SerializedName("backdrop_path")
    @Expose
    private String backdropPath;
    @SerializedName("adult")
    @Expose
    private Boolean adult;

    @Expose
    private List<Genre> genres;

    @SerializedName("production_companies")
    private List<ProductionCompany> productionCompanies;
        * */
    }

    public FilmDB(Context context) {
        super(context);
    }

    public boolean checkEmpty(){
        int count = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME, null);
        try {
            if(cursor != null)
                if(cursor.getCount() > 0){
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
        }finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        if(count>0)
            return false;
        else
            return true;
    }


    private String[] getColumnsFilmQuery(String prefix){

        if(prefix == null)
            prefix = "";
        else
            prefix += ".";

        String[] columns = {
                prefix+Column.ID,
                prefix+Column.CATEGORY_ID,
                prefix+Column.TITLE,
                prefix+Column.VOTE_COUNT,
                prefix+Column.VOTE_AVERAGE,
                prefix+Column.POPULARITY,
                prefix+Column.POSTER_PATH,
                prefix+Column.BACKDROP_PATH,
                prefix+Column.ORIGINAL_LANGUAGE,
                prefix+Column.ORIGINAL_TITLE,
                prefix+Column.OVERVIEW,
                prefix+Column.RELEASE_DATE,
                prefix+Column.HOMEPAGE
        };
        return columns;
    }

    private Film populateFilmObject(Cursor c) {
        Film film = new Film();
        film.setId(c.getInt(0));
        film.setCategoryId(c.getInt(1));
        film.setTitle(c.getString(2));
        film.setVoteCount(c.getInt(3));
        film.setVoteAverage(c.getDouble(4));
        film.setPopularity(c.getDouble(5));
        film.setPosterPath(c.getString(6));
        film.setBackdropPath(c.getString(7));
        film.setOriginalLanguage(c.getString(8));
        film.setOriginalTitle(c.getString(9));
        film.setOverview(c.getString(10));
        film.setReleaseDate(c.getString(11));
        film.setHomePage(c.getString(12));
        return film;
    }

    public ArrayList<Film> getFilmsByCategory(int id) {

        ArrayList list = new ArrayList<Film>();
        this.openReadableDB();
        String where = Column.CATEGORY_ID + "= ?";
        String[] whereArgs = { String.valueOf(id) };
        String[] columns = getColumnsFilmQuery(null);

        Cursor c = db.query(TABLE_NAME, columns, where, whereArgs, null, null, null);

        try {
            while (c.moveToNext()) {
                list.add(populateFilmObject(c));
            }
        } finally {
            c.close();
            this.closeDB();
        }
        return list;
    }


    public ArrayList<Film> getFilmsById(int id) {

        ArrayList list = new ArrayList<Film>();
        this.openReadableDB();
        String where = Column.ID + "= ?";
        String[] whereArgs = { String.valueOf(id) };
        String[] columns = getColumnsFilmQuery(null);

        Cursor c = db.query(TABLE_NAME, columns, where, whereArgs, null, null, null);

        try {
            while (c.moveToNext()) {
                list.add(populateFilmObject(c));
            }
        } finally {
            c.close();
            this.closeDB();
        }
        return list;
    }


    public List<Film> getFilmsByCriteria(String criteria){

        ArrayList<Film> listReturn = new ArrayList<>();
        openReadableDB();
        String columnsQuery = getColumnsFilmQuery("f").toString();
        String query = "select "+columnsQuery+", g.name genre_name " +
                "from "+ TABLE_NAME +" f join "+ FilmGenreDB.TABLE_NAME +" fg on f."+Column.ID+"=fg."+FilmGenreDB.Column.FILM_ID+" join "+ GenreDB.TABLE_NAME +" g on fg."+FilmGenreDB.Column.GENRE_ID+"=g."+ GenreDB.Column.ID +
                " where f.title like %?%" +
                " order by f._id, g.name";

        String[] selectionArgs = {criteria};
        Cursor cursor = db.rawQuery(query, selectionArgs);

        try{
            while (cursor.moveToNext()){
                Film film = new Film();
                film.setId(cursor.getInt(0));
                film.setCategoryId(cursor.getInt(1));
            }
        }
        catch (Exception e){

        }
        finally {
            cursor.close();
            closeDB();
        }
        return null;
    }




    public boolean deleteTable(){
        boolean response = true;
        try{
            this.openWriteableDB();
            db.execSQL("delete from "+ FilmGenreDB.TABLE_NAME);
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e){
            response = false;
        }
        finally {
            this.closeDB();
        }
        return response;
    }



    public void saveBatch(List<Film> films) {
        deleteTable();
        new FilmDB.SaveBatchTask(db, films).execute();
    }



    private ContentValues genreMapperContentValues(Film film) {
        ContentValues cv = new ContentValues();
        cv.put(Column.ID, film.getId());
        cv.put(Column.CATEGORY_ID, film.getCategoryId());
        cv.put(Column.TITLE, film.getTitle());
        cv.put(Column.VOTE_COUNT, film.getVoteCount());
        //cv.put(Column.VIDEO, film.getVideo());
        cv.put(Column.VOTE_AVERAGE, film.getVoteAverage());
        cv.put(Column.POPULARITY, film.getPopularity());
        cv.put(Column.POSTER_PATH, film.getPosterPath());
        cv.put(Column.BACKDROP_PATH, film.getBackdropPath());
        cv.put(Column.ORIGINAL_LANGUAGE, film.getOriginalLanguage());
        cv.put(Column.ORIGINAL_TITLE, film.getOriginalTitle());
        cv.put(Column.OVERVIEW, film.getOverview());
        cv.put(Column.RELEASE_DATE, film.getReleaseDate());
        cv.put(Column.HOMEPAGE, film.getHomePage());
        return cv;
    }


    private class SaveBatchTask extends AsyncTask<Void, Void, Boolean> {

        private SQLiteDatabase db;
        private List<Film> films;

        public SaveBatchTask(SQLiteDatabase db, List<Film> films){
            this.db = dbHelper.getReadableDatabase();
            this.films = films;
        }

        @Override
        protected Boolean doInBackground(Void... args) {
            boolean response = true;
            for(Film g : films){
                long newFilmId = db.insert(TABLE_NAME, null, genreMapperContentValues(g));
                List<Integer> genreIds = g.getGenreIds();
                for(long genreId : genreIds){
                    db.insert(FilmGenreDB.TABLE_NAME, null, FilmGenreDB.filmGenreMapperContentValues(newFilmId, genreId));
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(db!=null){
                db.close();
            }
        }

    }



}
