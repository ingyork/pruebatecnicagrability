package com.grability.grability.data.model;

import java.util.List;

/**
 * Created by jorge.castro on 1/5/2018.
 */

public class GroupFilmGenre {
    private int groupId;
    private String groupName;
    private List<Film> films;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Film> getFilms() {
        return films;
    }

    public void setFilms(List<Film> films) {
        this.films = films;
    }
}
