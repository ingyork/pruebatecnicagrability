package com.grability.grability.data.services;

import com.grability.grability.application.dtos.ResponseDto;
import com.grability.grability.application.dtos.ResponseGenresDto;
import com.grability.grability.data.model.Film;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public interface MovieService {

    @GET("popular")
    Observable<ResponseDto> getPopularMovies(@Query("api_key") String apiKey);

    @GET("top_rated")
    Observable<ResponseDto> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("upcoming")
    Observable<ResponseDto> getUpcomingMovies(@Query("api_key") String apiKey);

    @GET("{movieId}")
    Observable<Film> getDetailMovie(@Path("movieId") long movieId, @Query("api_key") String apiKey);

    @GET("movie")
    Observable<ResponseDto> searchMovie(@Query("api_key") String apiKey, @Query("query") String query, @Query("include_adult") boolean includeAdult);

    @GET("list")
    Observable<ResponseGenresDto> getGenres(@Query("api_key") String apiKey);

}