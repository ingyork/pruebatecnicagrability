package com.grability.grability.view.categoriesList;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.grability.grability.R;
import com.grability.grability.data.model.Category;
import com.grability.grability.presenter.categoriesList.CategoryListPresenter;
import com.grability.grability.presenter.categoriesList.ICategoryListPresenter;
import com.grability.grability.view.BaseActivity;


import java.util.List;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class CategoryListActivity extends BaseActivity implements ICategoryListView {

    private CategoryListAdapter adapter;
    @BindView(R.id.recyclerViewCategories) RecyclerView recyclerViewCategories;
    @BindView(R.id.ivEmpty) ImageView ivEmpty;

    ICategoryListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        init();
    }

    private void init(){

        setTitle("Categories");

        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCategories.setHasFixedSize(true);

        presenter = new CategoryListPresenter(this);
        presenter.loadCategories();

        hideRecyclerview();
    }

    private void showRecyclerview(){
        ivEmpty.setVisibility(View.GONE);
        recyclerViewCategories.setVisibility(View.VISIBLE);
    }

    private void hideRecyclerview(){
        recyclerViewCategories.setVisibility(View.GONE);
        ivEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void loadCategories(List<Category> categories) {
        adapter = new CategoryListAdapter(categories);
        recyclerViewCategories.setAdapter(adapter);
        showRecyclerview();
    }

    @Override
    public void onFailConnectNetwork() {
        Toast.makeText(this, "There is no network connection", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyImage() {
        hideRecyclerview();
    }
}
