package com.grability.grability.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jorge.castro on 12/22/2017.
 */

public class Category implements Parcelable {
    private long id;
    private String name;
    private int imageResource;

    public Category(){}

    public Category(long id, String name, int imageResource){
        this.id = id;
        this.name = name;
        this.imageResource = imageResource;
    }

    protected Category(Parcel in) {
        readFromParcel(in);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int url) {
        this.imageResource = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeInt(imageResource);
    }

    public void readFromParcel(Parcel parcel){
        id = parcel.readLong();
        name =  parcel.readString();
        imageResource = parcel.readInt();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
