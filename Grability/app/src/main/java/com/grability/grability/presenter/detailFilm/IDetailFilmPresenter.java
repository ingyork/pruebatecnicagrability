package com.grability.grability.presenter.detailFilm;

import com.grability.grability.presenter.BasePresenter;
import com.grability.grability.view.detailFilm.IDetailFilmView;

/**
 * Created by jorge.castro on 12/26/2017.
 */

public interface IDetailFilmPresenter extends BasePresenter<IDetailFilmView> {
    void showDetailFilm(long filmId);
}