package com.grability.grability.data.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class BaseEntities {

    protected SQLiteDatabase db;
    protected DBHelper dbHelper;
    protected Context context;

    public BaseEntities(Context context){
        dbHelper = new DBHelper(context);
        this.context = context;
    }

    protected void openReadableDB() {
        db = dbHelper.getReadableDatabase();
    }

    protected void openWriteableDB() {
        db = dbHelper.getWritableDatabase();
    }

    protected void closeDB() {
        if(db!=null){
            db.close();
        }
    }

}
