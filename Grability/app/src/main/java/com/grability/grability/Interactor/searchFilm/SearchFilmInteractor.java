package com.grability.grability.Interactor.searchFilm;

import android.support.annotation.NonNull;

import com.grability.grability.GrabilityApplication;
import com.grability.grability.data.db.GenreDB;
import com.grability.grability.data.model.Film;
import com.grability.grability.data.model.Genre;
import com.grability.grability.data.model.GroupFilmGenre;
import com.grability.grability.data.services.MovieRetrofitFactory;
import com.grability.grability.data.services.MovieService;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.application.util.Prefs;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class SearchFilmInteractor implements ISearchFilmInteractor {

    OnListenerSearchFilm listener;

    @Override
    public void setListener(OnListenerSearchFilm listener) {
        this.listener = listener;
    }

    @Override
    public void searchFilms(String searchPattern) {

        String apiKey = Prefs.getString(PreferencesUtils.KeyPreferences.API_KEY, "");

        MovieService service = MovieRetrofitFactory.createSearch();
        Disposable disposable = service.searchMovie(apiKey, searchPattern, false)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseDto -> {
                    listener.onSuccessSearchFilms( getGroupFilmGenre(responseDto.getResults()) );
                });
    }

    private List<GroupFilmGenre> getGroupFilmGenre(List<Film> films){
        List<Integer> genreIds = getGenreIdsFromRequest(films);
        List<GroupFilmGenre> response = getGroupNameFilmGenre(genreIds);
        for(int i=0; i<response.size(); i++){
            List<Film> filmGroupList = getFilmsByGroupId(films, response.get(i).getGroupId());
            response.get(i).setFilms(filmGroupList);
        }
        return response;
    }

    private List<GroupFilmGenre> getGroupNameFilmGenre(List<Integer> genreIds){

        List<GroupFilmGenre> groupFilmGenres = new ArrayList<>();

        GenreDB db = new GenreDB(GrabilityApplication.getAppContext());
        StringBuilder stringBuilder = new StringBuilder();
        for(int genreId : genreIds){
            stringBuilder.append(genreId+",");
        }

        String queryIds = stringBuilder.toString();
        queryIds = queryIds.substring(0, queryIds.length() - 1);
        List<Genre> genres = db.getGenresByIds(queryIds);
        for(Genre genre : genres){
            GroupFilmGenre group = new GroupFilmGenre();
            group.setGroupId(genre.getId());
            group.setGroupName(genre.getName());
            groupFilmGenres.add(group);
        }
        return groupFilmGenres;
    }

    @NonNull
    private List<Film> getFilmsByGroupId(List<Film> films, int genreId) {
        List<Film> filmGroupList = new ArrayList<>();
        for(Film film : films){
            if(film.getGenreIds().size() > 0 && film.getGenreIds().get(0) == genreId){
                filmGroupList.add(film);
            }
        }
        return filmGroupList;
    }

    @NonNull
    private List<Integer> getGenreIdsFromRequest(List<Film> films) {
        List<Integer> genreIds = new ArrayList<>();
        for(Film film : films){
            if(film.getGenreIds().size() > 0){
                int genreId = film.getGenreIds().get(0);
                if(!genreIds.contains(genreId))
                    genreIds.add(genreId);
            }
        }
        return genreIds;
    }

}
