package com.grability.grability.Interactor.categoriesList;


import com.grability.grability.GrabilityApplication;
import com.grability.grability.R;
import com.grability.grability.data.db.CategoryDB;
import com.grability.grability.data.db.GenreDB;
import com.grability.grability.data.model.Category;
import com.grability.grability.data.services.MovieRetrofitFactory;
import com.grability.grability.data.services.MovieService;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.application.util.Prefs;
import com.grability.grability.application.util.Util;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public class CategoryListInteractor implements ICategoryListInteractor {

    OnCategoryListener listener;

    @Override
    public void setListener(OnCategoryListener listener) {
        this.listener = listener;
    }

    @Override
    public void loadCategories() {

        if(Util.isOnline()){

            String apiKey = Prefs.getString(PreferencesUtils.KeyPreferences.API_KEY, "");

            MovieService service = MovieRetrofitFactory.createGenres();
            Disposable disposable = service.getGenres(apiKey)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(genres -> {

                        GenreDB dbGenre = new GenreDB(GrabilityApplication.getAppContext());
                        dbGenre.saveBatch(genres.getGenres());
                        requestCategories();
                    }, error -> {
                        String a = error.getMessage();
                    });
        }
        else{

            listener.onFailConnectNetwork();

            GenreDB dbGenre = new GenreDB(GrabilityApplication.getAppContext());
            if(dbGenre.checkEmpty()){
                listener.onEmptyCategories();
            }
            else{
                requestCategories();
            }

        }



    }




    private void requestCategories(){
        List<Category> categories = new ArrayList<>();

        CategoryDB db = new CategoryDB(GrabilityApplication.getAppContext());
        if(db.checkEmpty()){
            categories.add(new Category(1, "Popular", R.drawable.popular_movie));
            categories.add(new Category(2, "Top Rated", R.drawable.toprated_movie));
            categories.add(new Category(3, "Upcoming", R.drawable.upcoming_movie));

            db.saveBatch(categories);
        }
        else{
            categories = db.getCategories();
        }

        listener.onSuccessLoadCategories(categories);
    }

}
