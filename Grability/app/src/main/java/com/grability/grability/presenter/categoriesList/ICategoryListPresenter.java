package com.grability.grability.presenter.categoriesList;

import com.grability.grability.presenter.BasePresenter;
import com.grability.grability.view.categoriesList.ICategoryListView;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public interface ICategoryListPresenter extends BasePresenter<ICategoryListView> {
    void loadCategories();
    void showEmptyImage();
}
