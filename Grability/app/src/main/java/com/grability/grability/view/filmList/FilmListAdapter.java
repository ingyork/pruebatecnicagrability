package com.grability.grability.view.filmList;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.grability.grability.R;
import com.grability.grability.data.model.Film;
import com.grability.grability.view.detailFilm.DetailFilmListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorge.castro on 12/22/2017.
 */

public class FilmListAdapter extends RecyclerView.Adapter<FilmListAdapter.FilmListViewHolder> {

    private List<Film> data;
    private Context context;
    private LayoutInflater inflater;
    DetailFilmListener callbackFilmListAdapter;

    public FilmListAdapter(List<Film> data){
        this.data = data;
    }

    public void setCallbackFilmListAdapter(DetailFilmListener callbackFilmListAdapter){
        this.callbackFilmListAdapter = callbackFilmListAdapter;
    }

    @Override
    public FilmListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_film_list, parent, false);
        FilmListViewHolder holder = new FilmListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FilmListViewHolder holder, int position) {
        Film film = data.get(position);

        Uri uri = Uri.parse("https://image.tmdb.org/t/p/w500" + film.getPosterPath());
        Picasso.with(context).load(uri)
                .into(holder.imgCover);

        holder.filmTitle.setText(film.getTitle());

        double rating = film.getVoteAverage() * 10 / 20;
        holder.ratingBar.setRating((float)rating);

        holder.ratingBarText.setText(String.valueOf(rating));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class FilmListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txtTitle) TextView filmTitle;
        @BindView(R.id.imgCover) ImageView imgCover;
        @BindView(R.id.ratingBar) RatingBar ratingBar;
        @BindView(R.id.ratingBarText) TextView ratingBarText;

        public FilmListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Film film = data.get(getAdapterPosition());
            callbackFilmListAdapter.launchActivityForDetail(film.getId());
        }
    }
}
