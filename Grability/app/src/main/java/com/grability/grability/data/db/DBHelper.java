package com.grability.grability.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by jorge.castro on 11/19/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static int version = 1;
    private static String name = "grability_db" ;
    private static SQLiteDatabase.CursorFactory factory = null;

    public DBHelper(Context context){
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createCategoryTable(db);
        createFilmTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void createCategoryTable(SQLiteDatabase db){
        db.execSQL(CategoryDB.QUERY_CREATE);
    }

    private void createFilmTable(SQLiteDatabase db){
        db.execSQL(GenreDB.QUERY_CREATE);
        db.execSQL(FilmDB.QUERY_CREATE);
        db.execSQL(FilmGenreDB.QUERY_CREATE);
    }
}
