package com.grability.grability.presenter.detailFilm;

import com.grability.grability.Interactor.detailFilm.DetailFilmInteractor;
import com.grability.grability.Interactor.detailFilm.IDetailFilmInteractor;
import com.grability.grability.data.model.Film;
import com.grability.grability.view.detailFilm.IDetailFilmView;

/**
 * Created by jorge.castro on 12/26/2017.
 */

public class DetailFilmPresenter implements IDetailFilmPresenter, IDetailFilmInteractor.OnDetailFilmListener {

    private IDetailFilmView view;
    private IDetailFilmInteractor interactor;

    public DetailFilmPresenter(IDetailFilmView view){
        addView(view);
        interactor = new DetailFilmInteractor();
        interactor.setListener(this);
    }

    @Override
    public void addView(IDetailFilmView view) {
        this.view = view;
    }

    @Override
    public void removeView() {

    }

    @Override
    public void showDetailFilm(long filmId) {
        interactor.showDetailFilm(filmId);
    }

    @Override
    public void onDetailFilmListener(Film film) {
        view.showDetailFilm(film);
    }
}
