package com.grability.grability.Interactor.filmsList;

import com.grability.grability.data.model.Film;

import java.util.List;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public interface IFilmListInteractor {

    interface OnFilmListener {
        void onSuccessLoadFilms(List<Film> films);
        void onEmptyFilms();
    }

    void setListener(OnFilmListener listener);
    void loadPopularFilms();
    void loadTopRatedFilms();
    void loadUpcomingFilms();
}
