package com.grability.grability.view.categoriesList;

import com.grability.grability.data.model.Category;

import java.util.List;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public interface ICategoryListView {
    void loadCategories(List<Category> categories);
    void onFailConnectNetwork();
    void showEmptyImage();
}
