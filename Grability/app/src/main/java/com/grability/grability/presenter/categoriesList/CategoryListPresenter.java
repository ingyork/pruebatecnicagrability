package com.grability.grability.presenter.categoriesList;


import com.grability.grability.Interactor.categoriesList.CategoryListInteractor;
import com.grability.grability.Interactor.categoriesList.ICategoryListInteractor;
import com.grability.grability.data.model.Category;
import com.grability.grability.view.categoriesList.ICategoryListView;

import java.util.List;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public class CategoryListPresenter implements ICategoryListPresenter, ICategoryListInteractor.OnCategoryListener {

    private ICategoryListInteractor interactor;
    private ICategoryListView view;

    public CategoryListPresenter(ICategoryListView view){
        addView(view);
        interactor = new CategoryListInteractor();
        interactor.setListener(this);
    }

    @Override
    public void addView(ICategoryListView view) {
        this.view = view;
    }

    @Override
    public void removeView() {

    }

    @Override
    public void loadCategories() {
        interactor.loadCategories();
    }

    @Override
    public void showEmptyImage() {
        view.showEmptyImage();
    }


    @Override
    public void onSuccessLoadCategories(List<Category> categories) {
        view.loadCategories(categories);
    }

    @Override
    public void onEmptyCategories() {
        view.showEmptyImage();
    }

    @Override
    public void onFailConnectNetwork() {
        view.onFailConnectNetwork();
    }
}
