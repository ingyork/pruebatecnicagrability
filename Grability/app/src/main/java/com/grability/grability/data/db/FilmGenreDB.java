package com.grability.grability.data.db;

import android.content.ContentValues;
import android.content.Context;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class FilmGenreDB extends BaseEntities {

    public static String TABLE_NAME = "film_genres";
    public static final String QUERY_CREATE = "CREATE TABLE "+TABLE_NAME+"(" +
            " "+ Column.FILM_ID+" INTEGER NOT NULL," +
            " "+ Column.GENRE_ID+" INTEGER NOT NULL," +
            " PRIMARY KEY("+Column.FILM_ID+", "+Column.GENRE_ID+")," +
            " FOREIGN KEY ("+Column.FILM_ID+") REFERENCES "+FilmDB.TABLE_NAME+"("+FilmDB.Column.ID+")," +
            " FOREIGN KEY ("+Column.GENRE_ID+") REFERENCES "+GenreDB.TABLE_NAME+"("+GenreDB.Column.ID+"));" +
            "CREATE UNIQUE INDEX username ON user(username ASC)";

    public class Column {
        public static final String FILM_ID = "film_id";
        public static final String GENRE_ID = "genre_id";
    }

    public FilmGenreDB(Context context) {
        super(context);
    }


    public static ContentValues filmGenreMapperContentValues(long filmId, long genreId) {
        ContentValues cv = new ContentValues();
        cv.put(Column.FILM_ID, filmId);
        cv.put(Column.GENRE_ID, genreId);
        return cv;
    }

    public boolean deleteTable(){
        boolean response = true;
        try{
            this.openWriteableDB();
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e){
            response = false;
        }
        finally {
            this.closeDB();
        }
        return response;
    }
}