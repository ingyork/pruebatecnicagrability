package com.grability.grability.Interactor.categoriesList;

import com.grability.grability.data.model.Category;

import java.util.List;

/**
 * Created by jorge.castro on 12/23/2017.
 */

public interface ICategoryListInteractor {

    interface OnCategoryListener {
        void onSuccessLoadCategories(List<Category> categories);
        void onEmptyCategories();
        void onFailConnectNetwork();
    }

    void setListener(OnCategoryListener listener);
    void loadCategories();
}
