package com.grability.grability.Interactor.detailFilm;

import com.grability.grability.data.model.Film;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public interface IDetailFilmInteractor {

    interface OnDetailFilmListener {
        void onDetailFilmListener(Film film);
    }

    void setListener(OnDetailFilmListener listener);
    void showDetailFilm(long filmId);
}
