package com.grability.grability.presenter.filmList;

import com.grability.grability.Interactor.filmsList.FilmListInteractor;
import com.grability.grability.Interactor.filmsList.IFilmListInteractor;
import com.grability.grability.data.model.Film;
import com.grability.grability.view.filmList.IFilmListView;

import java.util.List;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public class FilmListPresenter implements IFilmListPresenter, IFilmListInteractor.OnFilmListener {

    private IFilmListInteractor interactor;
    private IFilmListView view;

    public FilmListPresenter(IFilmListView view) {
        addView(view);
        interactor = new FilmListInteractor();
        interactor.setListener(this);
    }

    @Override
    public void addView(IFilmListView view) {
        this.view = view;
    }

    @Override
    public void removeView() {

    }

    @Override
    public void loadFilmsByCategory(int categoryId) {

        switch (categoryId){
            case 1:{
                interactor.loadPopularFilms();
                break;
            }
            case 2: {
                interactor.loadTopRatedFilms();
                break;
            }
            case 3: {
                interactor.loadUpcomingFilms();
                break;
            }
        }
    }

    @Override
    public void onSuccessLoadFilms(List<Film> films) {
        view.loadFilmByCategory(films);
    }

    @Override
    public void onEmptyFilms() {

    }
}
