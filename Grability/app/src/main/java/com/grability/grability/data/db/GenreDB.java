package com.grability.grability.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.grability.grability.data.model.Category;
import com.grability.grability.data.model.Genre;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class GenreDB extends BaseEntities {

    public static String TABLE_NAME = "genre";
    public static final String QUERY_CREATE = "CREATE TABLE "+TABLE_NAME+"(" +
                                                " "+ Column.ID+" INTEGER PRIMARY KEY," +
                                                " "+ Column.NAME+" TEXT NOT NULL);";

    public class Column {
        public static final String ID = "_id";
        public static final String NAME = "name";
    }


    public GenreDB(Context context) {
        super(context);
    }


    public boolean checkEmpty(){
        int count = 0;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM " + TABLE_NAME, null);
        try {
            if(cursor != null)
                if(cursor.getCount() > 0){
                    cursor.moveToFirst();
                    count = cursor.getInt(0);
                }
        }finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        if(count>0)
            return false;
        else
            return true;
    }



    public boolean deleteTable(){
        boolean response = true;
        try{
            this.openWriteableDB();
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e){
            response = false;
        }
        finally {
            this.closeDB();
        }
        return response;
    }



    public ArrayList<Genre> getGenres() {
        ArrayList list = new ArrayList<Category>();
        this.openReadableDB();
        Cursor c = db.query(TABLE_NAME, null, null, null, null, null, null);

        try {
            while (c.moveToNext()) {
                Genre genre = new Genre();
                genre.setId(c.getInt(0));
                genre.setName(c.getString(1));
                list.add(genre);
            }
        } finally {
            c.close();
            this.closeDB();
        }
        return list;
    }

    public ArrayList<Genre> getGenresByIds(String ids) {
        ArrayList list = new ArrayList<Category>();
        this.openReadableDB();
        String query = "select "+Column.ID+", "+Column.NAME+" from "+TABLE_NAME+" where _id in ("+ids+") order by "+Column.NAME+" asc";
        Cursor c = db.rawQuery(query, null);

        try {
            while (c.moveToNext()) {
                Genre genre = new Genre();
                genre.setId(c.getInt(0));
                genre.setName(c.getString(1));
                list.add(genre);
            }
        } finally {
            c.close();
            this.closeDB();
        }
        return list;
    }

    public void saveBatch(List<Genre> genres) {
        deleteTable();
        new SaveBatchTask(db, genres).execute();
    }



    private ContentValues genreMapperContentValues(Genre genre) {
        ContentValues cv = new ContentValues();
        cv.put(Column.ID, genre.getId());
        cv.put(Column.NAME, genre.getName());
        return cv;
    }


    private class SaveBatchTask extends AsyncTask<Void, Void, Boolean> {

        private SQLiteDatabase db;
        private List<Genre> genres;

        public SaveBatchTask(SQLiteDatabase db, List<Genre> genres){
            this.db = dbHelper.getReadableDatabase();
            this.genres = genres;
        }

        @Override
        protected Boolean doInBackground(Void... args) {
            boolean response = true;
            for(Genre g : genres){
                long rowID = db.insert(TABLE_NAME, null, genreMapperContentValues(g));
            }
            return response;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(db!=null){
                db.close();
            }
        }

    }


}
