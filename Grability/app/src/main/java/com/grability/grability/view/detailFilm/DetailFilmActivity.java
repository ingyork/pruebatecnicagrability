package com.grability.grability.view.detailFilm;

import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.grability.grability.R;
import com.grability.grability.data.model.Film;
import com.grability.grability.data.model.Genre;
import com.grability.grability.data.model.ProductionCompany;
import com.grability.grability.presenter.detailFilm.DetailFilmPresenter;
import com.grability.grability.presenter.detailFilm.IDetailFilmPresenter;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.view.BaseActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailFilmActivity extends BaseActivity implements IDetailFilmView {

    IDetailFilmPresenter presenter;

    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.imgCover) ImageView imgCover;
    @BindView(R.id.ratingBar) RatingBar ratingBar;
    @BindView(R.id.ratingBarText) TextView ratingBarText;
    @BindView(R.id.tvVoteCount) TextView tvVoteCount;
    @BindView(R.id.tvHomepage) TextView tvHomepage;
    @BindView(R.id.tvOverview) TextView tvOverview;
    @BindView(R.id.tvGenres) TextView tvGenres;
    @BindView(R.id.tvProductionCompanies) TextView tvProductionCompanies;

    @BindView(R.id.layoutDetailFilm) RelativeLayout layoutDetailFilm;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_film);
        ButterKnife.bind(this);

        init();
    }


    private void init(){

        // Enable the Up button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new DetailFilmPresenter(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String filmId = bundle.getString(PreferencesUtils.KeyPreferences.FILM_ID);
            presenter.showDetailFilm(Long.parseLong(filmId));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void showDetailFilm(Film film) {
        tvTitle.setText(film.getTitle());

        Uri uri = Uri.parse("https://image.tmdb.org/t/p/w600" + film.getBackdropPath());
        Picasso.with(this).load(uri)
                .into(imgCover);

        double rating = film.getVoteAverage() * 10 / 20;
        ratingBar.setRating((float)rating);

        ratingBarText.setText(String.valueOf(rating));

        String voteCount = String.valueOf(film.getVoteCount());
        tvVoteCount.setText(voteCount);

        tvHomepage.setText(film.getHomePage());
        tvOverview.setText(film.getOverview());

        tvGenres.setText(getFormatGenres(film.getGenres()));
        tvProductionCompanies.setText(getFormatProductionCompanies(film.getProductionCompanies()));



        showLayoutDetailFilm();
    }


    private void showLayoutDetailFilm(){
        progressBar.setVisibility(View.GONE);
        layoutDetailFilm.setVisibility(View.VISIBLE);
    }


    private String getFormatGenres(List<Genre> genres){
        StringBuilder response = new StringBuilder();
        boolean isFirst = true;
        for (Genre genre : genres){
            if (isFirst){
                isFirst = false;
                response.append(genre.getName());
            }
            else
                response.append(", " + genre.getName());
        }
        return response.toString();
    }


    private String getFormatProductionCompanies(List<ProductionCompany> productionCompanies){
        StringBuilder response = new StringBuilder();
        boolean isFirst = true;
        for (ProductionCompany productionCompany : productionCompanies){
            if (isFirst){
                isFirst = false;
                response.append(productionCompany.getName());
            }
            else
                response.append(", " + productionCompany.getName());
        }
        return response.toString();
    }

}
