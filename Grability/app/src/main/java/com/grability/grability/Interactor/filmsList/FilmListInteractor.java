package com.grability.grability.Interactor.filmsList;

import com.grability.grability.GrabilityApplication;
import com.grability.grability.data.db.FilmDB;
import com.grability.grability.data.model.Film;
import com.grability.grability.data.services.MovieRetrofitFactory;
import com.grability.grability.data.services.MovieService;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.application.util.Prefs;
import com.grability.grability.application.util.Util;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public class FilmListInteractor implements IFilmListInteractor {

    private OnFilmListener listener;

    @Override
    public void setListener(OnFilmListener listener) {
        this.listener = listener;
    }

    @Override
    public void loadPopularFilms() {

        int categoryId = 1;

        if(Util.isOnline()){

            FilmDB db = new FilmDB(GrabilityApplication.getAppContext());

            String apiKey = Prefs.getString(PreferencesUtils.KeyPreferences.API_KEY, "");

            MovieService service = MovieRetrofitFactory.create();
            Disposable disposable = service.getPopularMovies(apiKey)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseDto -> {
                        saveFilmsLocal(responseDto.getResults(), categoryId);
                        listener.onSuccessLoadFilms(responseDto.getResults());
                    });

        }
        else{
            FilmDB db = new FilmDB(GrabilityApplication.getAppContext());
            boolean isEmpty = db.checkEmpty();
            List<Film> films = db.getFilmsByCategory(categoryId);
            listener.onSuccessLoadFilms(films);
        }
    }

    @Override
    public void loadTopRatedFilms() {

        int categoryId = 2;

        if(Util.isOnline()){
            FilmDB db = new FilmDB(GrabilityApplication.getAppContext());
            String apiKey = Prefs.getString(PreferencesUtils.KeyPreferences.API_KEY, "");
            MovieService service = MovieRetrofitFactory.create();
            Disposable disposable = service.getTopRatedMovies(apiKey)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseDto -> {
                        saveFilmsLocal(responseDto.getResults(), categoryId);
                        listener.onSuccessLoadFilms(responseDto.getResults());
                    });
        }
        else{
            FilmDB db = new FilmDB(GrabilityApplication.getAppContext());
            List<Film> films = db.getFilmsByCategory(categoryId);
            listener.onSuccessLoadFilms(films);
        }

    }

    @Override
    public void loadUpcomingFilms() {

        int categoryId = 3;

        if(Util.isOnline()){
            String apiKey = Prefs.getString(PreferencesUtils.KeyPreferences.API_KEY, "");
            MovieService service = MovieRetrofitFactory.create();
            Disposable disposable = service.getUpcomingMovies(apiKey)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(responseDto -> {
                        saveFilmsLocal(responseDto.getResults(), categoryId);
                        listener.onSuccessLoadFilms(responseDto.getResults());
                    });
        }
        else{
            FilmDB db = new FilmDB(GrabilityApplication.getAppContext());
            List<Film> films = db.getFilmsByCategory(categoryId);
            listener.onSuccessLoadFilms(films);
        }
    }



    private void saveFilmsLocal(List<Film> films, int categoryId){
        FilmDB db = new FilmDB(GrabilityApplication.getAppContext());
        for(int i=0; i<films.size(); i++){
            films.get(i).setCategoryId(categoryId);
        }
        db.saveBatch(films);
    }
}