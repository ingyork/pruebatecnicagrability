package com.grability.grability.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.grability.grability.R;
import com.grability.grability.view.searchFilm.SearchFilmActivity;

public class BaseActivity extends AppCompatActivity {

    static final int REQUEST_CODE_SUCCESS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.miSearchMovie:
                Intent intent = new Intent(this, SearchFilmActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SUCCESS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
