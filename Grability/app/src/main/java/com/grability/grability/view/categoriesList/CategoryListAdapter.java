package com.grability.grability.view.categoriesList;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.grability.grability.R;
import com.grability.grability.data.model.Category;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.view.filmList.FilmListActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jorge.castro on 12/22/2017.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryListViewHolder> {

    private List<Category> data;
    private Context context;
    private LayoutInflater inflater;

    public CategoryListAdapter(List<Category> data){
        this.data = data;
    }

    @Override
    public CategoryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.item_categories_list, parent, false);
        CategoryListViewHolder holder = new CategoryListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CategoryListViewHolder holder, int position) {
        Category category = data.get(position);
        holder.categoryTitle.setText(category.getName());
        holder.categoryImage.setImageResource(category.getImageResource());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class CategoryListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.categoryTitle) TextView categoryTitle;
        @BindView(R.id.categoryImage) ImageView categoryImage;

        public CategoryListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int index = getAdapterPosition();
            String categoryId = String.valueOf(data.get(index).getId());

            Intent intent = new Intent(context.getApplicationContext(), FilmListActivity.class);
            intent.putExtra(PreferencesUtils.KeyPreferences.CATEGORY_ID, categoryId);
            intent.putExtra(PreferencesUtils.KeyPreferences.CATEGORY_NAME, data.get(index).getName());
            context.startActivity(intent);
        }
    }
}
