package com.grability.grability.view.searchFilm;

import com.grability.grability.data.model.GroupFilmGenre;

import java.util.List;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public interface ISearchFilmView {
    void showFilms(List<GroupFilmGenre> groupFilmGenres);
}
