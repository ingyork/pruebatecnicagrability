package com.grability.grability.data.model;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public class ProductionCompany {
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
