package com.grability.grability.presenter.searchFilm;

import com.grability.grability.presenter.BasePresenter;
import com.grability.grability.view.searchFilm.ISearchFilmView;

/**
 * Created by jorge.castro on 12/27/2017.
 */

public interface ISearchFilmPresenter extends BasePresenter<ISearchFilmView> {
    void searchFilms(String searchPattern);
}
