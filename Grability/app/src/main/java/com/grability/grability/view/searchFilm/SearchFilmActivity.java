package com.grability.grability.view.searchFilm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.grability.grability.R;
import com.grability.grability.application.util.PreferencesUtils;
import com.grability.grability.data.model.Film;
import com.grability.grability.data.model.GroupFilmGenre;
import com.grability.grability.presenter.searchFilm.ISearchFilmPresenter;
import com.grability.grability.presenter.searchFilm.SearchFilmPresenter;
import com.grability.grability.view.detailFilm.DetailFilmActivity;
import com.grability.grability.view.detailFilm.DetailFilmListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFilmActivity extends AppCompatActivity implements View.OnClickListener, ISearchFilmView, DetailFilmListener {

    static final int REQUEST_CODE_SUCCESS = 1;

    @BindView(R.id.rvSearch)
    RecyclerView recyclerView;

    @BindView(R.id.txtCriteria)
    TextView txtCriteria;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    SearchFilmSectionAdapter adapter;


    ISearchFilmPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_film);

        ButterKnife.bind(this);

        init();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init(){

        // Enable the Up button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //setLayout Manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);


        btnSearch.setOnClickListener(this);

        presenter = new SearchFilmPresenter(this);
    }


    @Override
    public void showFilms(List<GroupFilmGenre> groupFilmGenres) {

        //Create a List of Section DataModel implements Section
        List<SectionHeader> sections = new ArrayList<>();

        for(GroupFilmGenre group : groupFilmGenres){
            List<Film> films = group.getFilms();

            //Create a List of Child DataModel
            List<ChildItem> childList = new ArrayList<>();
            for (Film film : films){
                ChildItem childItem = new ChildItem(film.getId(), film.getTitle(), film.getBackdropPath(), film.getVoteAverage());
                childList.add(childItem);
            }
            sections.add(new SectionHeader(childList, group.getGroupName()));
        }

        adapter = new SearchFilmSectionAdapter(this, sections);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnSearch){
            String criteria = txtCriteria.getText().toString();
            if(criteria.equals("")){
                recyclerView.setAdapter(null);
            }
            else{
                presenter.searchFilms(criteria);
            }
        }
    }

    @Override
    public void launchActivityForDetail(long filmId) {
        Intent intent = new Intent(this, DetailFilmActivity.class);
        intent.putExtra(PreferencesUtils.KeyPreferences.FILM_ID, String.valueOf(filmId));
        startActivityForResult(intent, REQUEST_CODE_SUCCESS);
    }
}
