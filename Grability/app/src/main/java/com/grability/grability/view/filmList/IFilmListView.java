package com.grability.grability.view.filmList;

import com.grability.grability.data.model.Film;

import java.util.List;

/**
 * Created by jorge.castro on 12/24/2017.
 */

public interface IFilmListView {
    void loadFilmByCategory(List<Film> films);
}
