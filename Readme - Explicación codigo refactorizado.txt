En resumen la refactorización consistion en identificar una multiple responsabilida dentro de varios metodos, 
algunos metodos como: launchProductDetailFromStore, launchMultipleProductDetail, launchSingleProductDetail, entre otros; 
tienen como parametro "boolean updateView"; este al ser una bandera booleana le da 2 responsabilidades a los metodos, 
ejecutando una acción cuando es "true" y otra acción cuando es "false".

Cuando hacemos código limpio, debemos asegurarnos de que sea lo más sencillo, leible y manteniblemente posible, por lo que 
decidí separar en varios metodos estas responsabilidades y eliminar el parametro "updateView".


Dentro cree dos archivos llamados: "Codigo refactorizado" y "Codigo refactorizado con comentarios". Codigo refactorizado con comentarios, 
es el código con la refactorización pero sin borrar los metodos anteriores (solo se comentaron para ver la comparación del antes y el 
despues). Por otro lado el archivo llamado codigo refactorizado es la refactorización sin ningún tipo de comentarios y seria este el archivo, 
que se deberia usar para producción.